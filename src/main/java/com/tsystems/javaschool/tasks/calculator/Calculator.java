package com.tsystems.javaschool.tasks.calculator;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays; 
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        try {
        String evExpr = evalPostFix(getPostFix(statement));     
        
        return evExpr;
        } catch(Exception e) {
            return null;
        }
    }
    
    public String evalPostFix(String s) {
        Stack<Double> numStack = new Stack<>();
        List<String> listTokens = Arrays.asList(s.split(" "));
        listTokens.forEach((token) -> {
            if(isNumber(token)) {
                Double op = Double.parseDouble(token);
                numStack.push(op);                
            }
            else if(isOperator(token)) {
                Double num2 = numStack.pop();
                if(numStack.empty()) 
                    throw new EmptyStackException();
                else {
                    Double num1 = numStack.pop();
                    numStack.push(calcNum(token, num1, num2));
                }
            }
        });
        Double res = numStack.pop();
        
        return roundingRes(res);
    }
    
    public String getPostFix(String statement) {
        Stack<String> operatorStack = new Stack<>();
        List<String> result = new ArrayList<>();
        List<String> postFix = splitString(statement);
        postFix.forEach((token) -> {
            if(isNumber(token)) {
                result.add(token);
            }
            else if(token.equals("(")) {
                operatorStack.push(token);
            }
            else if(token.equals(")")) {
                String stToken = operatorStack.pop();
                while(!stToken.equals("(")) {
                    result.add(stToken);
                    stToken = operatorStack.pop();
                }
            }
            else if(isOperator(token)) {
                while(!operatorStack.empty() && priorityOp(operatorStack.peek()) >= priorityOp(token)){
                    result.add(operatorStack.pop());
                }
                operatorStack.push(token);          
                
            }
            else {
                throw new IllegalArgumentException();
            }
        });
        while(!operatorStack.empty()) {
            result.add(operatorStack.pop());
        }
        String resStr = String.join(" ", result);
        return resStr;    
    }
    
    public List<String> splitString(String statement) {        
        if(statement == null) {
            throw new IllegalArgumentException();
        }
        String[] symbols = {"+", "-", "*", "/", "(", ")"};
        for(String sym: symbols) {
            if(statement.contains(sym)) {
                statement = statement.replace(sym, " " + sym + " ");
            }
        }
        List<String> list = Arrays.asList(statement.split("\\s+"));
        return list;
    }
    
    public boolean isNumber(String s) {
        Pattern pattern = Pattern.compile("^[0-9]\\d*(\\.\\d+)?$");
        Matcher matcher = pattern.matcher(s);
        return matcher.find();
    }
    
    public boolean isOperator(String s) {
        switch(s) {
            case "+":
            case "-":
            case "*":
            case "/":
                return true;
            default:
                return false;
        }
    }   
    
    public Integer priorityOp(String s) {
        Map<String, Integer> map = new HashMap<>();
        map.put("(", 1);
        map.put("+", 2);
        map.put("-", 2);
        map.put("*", 3);
        map.put("/", 3);
        return map.get(s);
    }
    
    public Double calcNum(String opr, Double num1, Double num2) {
        switch(opr) {
            case "+": return num1 + num2;
            case "-": return num1 - num2;
            case "*": return num1 * num2;
            case "/":
                if(num2 != 0) {
                    return num1 / num2; 
                }
                else 
                    throw new ArithmeticException();
            default: return null;
        }    
    }
    
    public String roundingRes(Double number) {
        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.HALF_UP);
        String str = df.format(number);
        return str.replace(",", ".");
    }

}
