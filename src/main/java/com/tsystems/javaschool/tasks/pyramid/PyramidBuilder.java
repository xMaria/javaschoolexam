package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        isNullElem(inputNumbers);
        int sizeRow = appropriateElemAmount(inputNumbers);
        int sizeCol = 2 * sizeRow - 1;
        inputNumbers.sort(Comparator.naturalOrder());
        ListIterator<Integer> listNumbers = inputNumbers.listIterator();
        int[][] finalPyramid = new int[sizeRow][sizeCol];
        int blank;
        
        for(int i = 0; i < sizeRow; i++) {
            int k = i + 1;
            blank = 0;
            for(int j = 0; j < sizeCol; j++) {
                if((i + j)  >= (sizeRow - 1)) {
                    if(k > 0) {
                        if(blank == 0) {                            
                            finalPyramid[i][j] = listNumbers.next();
                            blank = 1;
                            k--;
                        }
                        else
                            blank = 0;
                    }
                }
            }
        }        
        return finalPyramid;
    }
    
    public int appropriateElemAmount(List<Integer> inputNumbers) {
        double elemAmount = (double)inputNumbers.size();
        double requiredAmount = (Math.sqrt(1 + 8 * elemAmount) - 1) / 2;
        if(getFractPart(requiredAmount) == 0)
            return (int)requiredAmount;
        else
            throw new CannotBuildPyramidException();        
    }
    
    public double getFractPart(double numDouble) {
        int numInt = (int)numDouble;
        return numDouble - numInt;
    }
    
    public void isNullElem(List<Integer> list) {
        int posOfNull = list.indexOf(null);
        if(posOfNull != -1) {
            throw new CannotBuildPyramidException();
        }
    }
}
